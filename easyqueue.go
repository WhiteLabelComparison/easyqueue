package easyqueue

import (
	"fmt"
	"log"
	"time"

	"github.com/adjust/rmq"
)

var con rmq.Connection

type Runnable func(string)

func Connect(c string)  {
	con = rmq.OpenConnection("consumer", "tcp", c, 2)
	log.Printf("Connection Opened to %s", c)
}

func RegisterQueue(qn string) *Queue {
	return &Queue{
		Connection: con.OpenQueue(qn),
	}
}

type Queue struct {
	Connection rmq.Queue
}

func (queue *Queue) Listen(n int, run Runnable) *Queue {
	queue.Connection.StartConsuming(1000, 500*time.Millisecond)

	// Create the required number of listeners
	for i := 0; i < n; i++ {
		name := fmt.Sprintf("listener%d", i)

		go queue.Connection.AddConsumer(name,NewListener(i, run))
		log.Printf("Adding %s", name)
	}

	return queue
}

func (queue *Queue) Publish(p string) *Queue {
	queue.Connection.Publish(p)

	return queue
}

// Listener is the format required to create listeners
type Listener struct {
	name   string
	count  int
	before time.Time
	callback Runnable
}

// NewListener creates a new struct holding details of the queue listener
func NewListener(tag int, run Runnable) *Listener {
	return &Listener{
		name:   fmt.Sprintf("listener%d", tag),
		count:  0,
		before: time.Now(),
		callback: run,
	}
}


func (listener *Listener) Consume(delivery rmq.Delivery) {
	listener.callback(delivery.Payload())
}